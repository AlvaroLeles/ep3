# README

```
Álvaro Leles Guimarães - 18/0096991 - Turma B (Carla Rocha)
Ian Fillipe Pontes Ferreira - 18/0102087 - Turma B (Carla Rocha)
```

# Descrição do produto
O produto é um site em formato de blog com tema sensacionalista, que tem como objetivo, por meio de posts sarcásticos, propor humor em cima de temas relacionados a UnB.
* OBS: Publicações contendo desrespeito, discurso de ódio ou derivados podem facilmente ser excluídos por um usuário administrador, além de ser possível excluir o próprio usuário

# Descrição do projeto
* O projeto é um blog onde qualquer usuário pode ver as publicações e os usuários existentes e pode se inscrever para publicar no blog.
* Um usuário comum já cadastrado no blog pode, além das funcionalidades já mencionadas, criar publicações, editar suas próprias publicações e excluí-las.
* Já um usuário administrador pode, além de tudo que um usuário comum faz, editar e excluir qualquer publicação, além de poder excluir usuários.
* Para entrar como administrador (no site online), usar o login:

```email```: admin@exemplo.com

```Password```: senha

# Detalhes interessantes
* Por termos implementado o conceito de deploy, o site, além de funcionar no próprio computador usando o "localhost", também se encontra online, no endereço: "http://quebrando-o-teu.herokuapp.com/". (Há alguns exemplos de publicação no site online que mostram o objetivo do blog)
* O programa cuida de validações como: não permitir publicações sem título ou descrição (nem artigos e descrições muito grandes ou muito pequenas), não permitir publicações sem usuário associado, não permitir cadastro de usuários sem username, ou com username já existente em banco, apenas permitir cadastro de usuário com email que segue um formato "padrão" por meio de regex e que já não exista em banco.
* Há paginação no site para que não seja necessário rolar a tela e se distanciar demais do topo da página.
* É possível editar seus próprios dados depois de cadastrado.
* Relação user-article: "has many"
* Relação article-user: "belongs to"

# Diagrama de Classe - Models
![](DiagramaModels.png)

# Diagrama de Classe - Controllers
![](DiagramaControllers.png)

# Diagrama - Casos de Uso
![](DiagramaCasosDeUso.png)

# Versões utilizadas

```ruby
ruby '2.6.3'
rails '6.0.1'
```

# Como rodar o programa:

* Clonar o repositório

* No terminal, onde a pasta foi clonada, executar os comandos:
```ruby
bundle install --without production
yarn install --check-files
rails db:migrate
```
Para instalar as gems utilizadas no projeto

* Para abrir o sevidor:
```ruby
rails s
```

* Em seu navegador, colocar o link:
```ruby
http://localhost:3000
```
* Para criar um usuário administrador, cadastre um usuário comum por meio do site gerado, e depois, no terminal, abra o console rails através do comando 'rails c', pegue o usuário cadastrado com 'user = User.last', defina seu atributo 'admin' como true ('user.admin = true') e salve no banco de dados ('user.save')

# Gems utilizadas

```ruby
gem 'bcrypt', '~> 3.1.7'
gem 'will_paginate', '3.1.7'
gem 'bootstrap-will_paginate', '0.0.10'
gem 'puma', '~> 4.1'
gem 'bootstrap-sass', '~> 3.4.1'
gem 'sass-rails', '>= 6'
gem 'webpacker', '~> 4.0'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.7'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'sqlite3', '~> 1.4'
gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
gem 'web-console', '>= 3.3.0'
gem 'listen', '>= 3.0.5', '< 3.2'
gem 'spring'
gem 'spring-watcher-listen', '~> 2.0.0'
gem 'capybara', '>= 2.15'
gem 'selenium-webdriver'
gem 'webdrivers'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'pg'
gem 'rails_12factor'
```